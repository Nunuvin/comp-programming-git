#include<bits/stdc++.h>

using namespace std;

int treesize(int depth){
    int total =0;
    for(int i=0;i<=depth;i++){
        total += pow(2,i);
    }
    return total;
}

int main(){
    int depth;
    cin >> depth;
    string path;
    cin >> path;
    
    int nodes = treesize(depth);
//   int tree[nodes];
    
//    for(int i=0;i<nodes;i++) tree[i]=nodes -i;
    int loc = 1;

    for(char c : path){
        if(c == 'L') loc = 2*loc;
        else loc = 2*loc+1;
//        cout << c << " " << loc << endl;

    }
    cout<< nodes-(loc-1)<<endl;

    return 0;
}
