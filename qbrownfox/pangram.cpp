#include<bits/stdc++.h>
using namespace std;

void sortString(string &str) 
{ 
   sort(str.begin(), str.end()); 
   cout << str; 
} 

int main(){
    unordered_map<char, bool> letters;
    string in;
    getline(cin,in);
    int lines = stoi(in);
    for(int line=0;line<lines;line++){
        for(int i=(int)'a';i<(int)'z'+1;i++){
            letters[(char)i] = false;

        }
    
        getline(cin, in);
        for(int i=0;i<in.size();i++){
            if(isalpha(in[i])){
                //cout << in[i];
                in[i] = tolower(in[i]);
                if(letters[in[i]] != true){
                    letters[in[i]] = true;
                }
            }    
        }
        bool pangram = true;
        for(unordered_map<char,bool>::iterator it = letters.begin();it!=letters.end();++it){
            if(it->second == false){
                pangram = false;
            }
        }
        if(pangram){
            cout << "pangram";
        }else{
            cout << "missing ";
        }
        string mis;
        for(unordered_map<char,bool>::iterator it = letters.begin();it!=letters.end();++it){
            if(it->second != true){
                mis+=(char) tolower(it->first);
            }
        }
        sortString(mis);
        cout << endl;
    }
    return 0;
}
