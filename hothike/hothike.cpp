#include<bits/stdc++.h>

using namespace std;

int main(){
    int n;
    cin >> n;
    int days[n];
    for(int i=0;i<n;i++) cin >>days[i];
    int start =0;
    int lowMax = INT_MAX;
    for(int i=0;i<n-2;i++){
        int curMax = max(days[i],days[i+2]);
           if(curMax < lowMax){
               start = i;
               lowMax = curMax;
           }

    }
    cout << start+1 << " " << lowMax << endl;
    return 0;
}
