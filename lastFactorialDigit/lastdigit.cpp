#include <iostream>

using namespace std;

int fac(int a){
    int prod = 1;
    for(int i=2;i<=a;i++){
        prod = (i * prod) % 10;
    }
    return prod;
}

int main() {
    int tests;
    cin >> tests;
    for( int i=0;i<tests;i++){
        int a;
        cin >> a;
        cout << fac(a) << endl;
        
    }
}