// the idea is tree stores parent at index
// hop to parent till you get to root as it will have 0 as its parent
#include<bits/stdc++.h>

using namespace std;

int main(){
    int a,n;
    cin >> a; //kitten
    cin.ignore();
    int tree[100]={0};
    
    while(cin >> n && n != -1){
        string s;
        getline(cin, s);

        stringstream l(s);

        int t;
        while(l >> t){
            tree[t] = n;
        }
    }

    while(a != 0){
        cout << a << " ";
        a = tree[a];
    }
    cout << endl;
    return 0;
}
