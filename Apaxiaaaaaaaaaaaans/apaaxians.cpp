#include <iostream>
#include <string>
using namespace std;

int main() {
    string s;
    cin >> s;
    char p = ' ';
    for( char c: s){
        if(c == p) continue;
        cout << c;
        p = c;
    }
}