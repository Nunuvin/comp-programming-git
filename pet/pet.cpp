#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main(){
    string l;
    string buf;

    int max[2]={-1}; // index sum
    for(int i=0;i<5;i++){
        int total = 0 ;
        getline(cin, l);
        stringstream ss(l);
        while(ss >> buf){
           total += stoi(buf);
        }
        if (total > max[1]){
            max[0] = i;
            max[1] = total;
        }

    }
    cout << max[0]+1 << " " << max[1]<<endl;

    return 0;
}