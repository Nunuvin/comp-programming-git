#include <iostream>


using namespace std;

int main(){
    int total, booked, t;
    cin >> total >> booked;
    int r[total]={0};

    if(total == booked){
        cout << "too late" << endl;
        return 0;
    }
    for(int i=0;i<booked;i++){
        cin >> t;
        //cout << "booked "<< t<<endl;
        r[t-1] = 1;
    }
    for(int i=0;i<total;i++){
        if(r[i] == 0){
            cout << i+1 << endl;
            break;
        }
    }

    return 0;
}